<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["logueado"]) || $_SESSION["logueado"] !== true) {
    header("location: marscloud.php");
    exit;
}


// Setear variables vacias para evitar errores al usarlas en run-instances
$EMPRESA = '';
$wordpress = '';
$moodle = '';
$nextcloud = '';
$ftp = '';
$wiki = '';
$jira = '';
$joomla = '';



// Coger nombre de empresa y si variables de servicios estan seteadas, cambiar valor a "true"
if (isset($_POST["empresa"]) && !empty($_POST["empresa"])) {
    $EMPRESA = $_POST["empresa"];
}

if (isset($_POST["wordpress"]) && !empty($_POST["wordpress"])) {
    $wordpress = "true";
}
if (isset($_POST["moodle"]) && !empty($_POST["moodle"])) {
    $moodle = "true";
}
if (isset($_POST["nextcloud"]) && !empty($_POST["nextcloud"])) {
    $nextcloud = "true";
}
if (isset($_POST["ftp"]) && !empty($_POST["ftp"])) {
    $ftp = "true";
}
if (isset($_POST["wiki"]) && !empty($_POST["wiki"])) {
    $wiki = "true";
}
if (isset($_POST["jira"]) && !empty($_POST["jira"])) {
    $jira = "true";
}
if (isset($_POST["joomla"]) && !empty($_POST["joomla"])) {
    $joomla = "true";
}

/*
// Cargar vendor AWS
require 'aws/aws-autoloader.php';

// Usar modulo de cliente EC2
use Aws\Ec2\Ec2Client;

// Crear nuevo cliente que cogerá las credenciales de .aws/credentials (usar siempre region us-east-1)
$client = new Aws\Ec2\Ec2Client([
    'region' => 'us-east-1',
    'version' => '2016-11-15',
    'profile' => 'default'
]);

// Ejecutar run-instances
$result = $client->runInstances([  
// Definir ID de launch template 
'LaunchTemplate' => [
    'LaunchTemplateId' => 'lt-00f47af9cb356b1e2'        
],
// Definir numero de instancias a levantar
'MaxCount' => 1,
'MinCount' => 1,
// Definir tags que cogeran valores de variables recogidas de formulario
'TagSpecifications' => [
    [
        'ResourceType' => 'instance',
        'Tags' => [
            [
                'Key' => 'EMPRESA',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'Name',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'wordpress',
                'Value' => $wordpress,
            ],
            [
                'Key' => 'moodle',
                'Value' => $moodle,
            ],
            [
                'Key' => 'nextcloud',
                'Value' => $nextcloud,
            ],
            [
                'Key' => 'ftp',
                'Value' => $ftp,
            ],
            [
                'Key' => 'mediawiki',
                'Value' => $mediawiki,
            ],
            [
                'Key' => 'jira',
                'Value' => $jira,
            ],
            [
                'Key' => 'rocketchat',
                'Value' => $rocketchat,
            ],
            [
                'Key' => 'joomla',
                'Value' => $joomla,
            ],
           
        ],
    ],
],
]);
*/
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link href="marscloud.css" rel="stylesheet">
    <style type="text/css">
        body {
            font: 14px sans-serif;
            text-align: center;
            width: 100vw;
            height: 100vh
        }

        .wrapper {
            width: 350px;
            padding: 20px;
        }
        .url-servei {
            width: 350px;
            padding: 5px;
            color: white;
            background-color: rgba(255, 255, 255, 0.2);  
        }
    </style>
</head>

<body class="d-flex flex-column justify-content-center align-items-center">
    <div class="wrapper">
        <h2><img src="logomars.png" width="200" height="200"></h2>
        <div class="url-servei">


            <?php
            echo "<h3>El teu domini principal és </h3>";
            echo '<a href="https://' . $EMPRESA . '.marscloudsolutions.com"> <h3>' . $EMPRESA . '.marscloudsolutions.com</h3>';
            ?>

        </div>

        <div class="mt-4">
            <a href="marscloudmain.php" class="btn btn-primary">Crear una altre servei</a>
        </div>
        <div class="mt-4">
            <a href="marscloudlogout.php" class="btn btn-danger">Tanca la sessió</a>
        </div>

        <div class="underlay-photo"></div>
        <div class="underlay-black"></div>
    </div>
</body>

</html>