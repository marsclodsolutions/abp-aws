<?php
   require 'aws/aws-autoloader.php';
    
    use Aws\Ec2\Ec2Client;
    
    $client = new Aws\Ec2\Ec2Client([
        'region' => 'us-east-1',
        'version' => '2016-11-15',
        'profile' => 'default'
    ]);

   $result = $client->runInstances([   
    'LaunchTemplate' => [
        'LaunchTemplateId' => 'lt-00f47af9cb356b1e2'        
    ],
    'MaxCount' => 1,
    'MinCount' => 1,
    'TagSpecifications' => [
        [
            'ResourceType' => 'instance',
            'Tags' => [
                [
                    'Key' => 'EMPRESA',
                    'Value' => 'prova1',
                ],
                [
                    'Key' => 'Name',
                    'Value' => 'prova1',
                ],
                [
                    'Key' => 'wordpress',
                    'Value' => 'true',
                ],
                [
                    'Key' => 'moodle',
                    'Value' => '',
                ],
                [
                    'Key' => 'nextcloud',
                    'Value' => '',
                ],
                [
                    'Key' => 'ftp',
                    'Value' => '',
                ],
            ],
        ],
    ],
]);
?>