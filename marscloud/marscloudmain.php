<?php
// Initialize the session
session_start();

// Check if the user is logged in, if not then redirect him to login page
if (!isset($_SESSION["logueado"]) || $_SESSION["logueado"] !== true) {
    header("location: marscloud.php");
    exit;
}
?>

<!DOCTYPE html>
<link rel="stylesheet" href="styles/marscloud.css">

<head>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <title>BENVINGUTS A MARSCLOUDSOLUTIONS! </title>
</head>

<body class="d-flex flex-column justify-content-center align-items-center">
    <div class="page-header">
        <h1>BENVINGUTS A MARSCLOUDSOLUTIONS! </h1>

    </div>

    <style type="text/css">
        body {
            font: 14px sans-serif;
            text-align: center;
            width: 100vw;
            height: 100vh
        }

        #wrapper {
            width: 600px;
            margin: 0 auto;
            font-family: Verdana, sans-serif;
        }

        legend {
            color: #0481b1;
            font-size: 16px;
            padding: 0 10px;
            background: #fff;
            -moz-border-radius: 4px;
            box-shadow: 0 1px 5px rgba(4, 129, 177, 0.5);
            padding: 5px 10px;
            text-transform: uppercase;
            font-family: Helvetica, sans-serif;
            font-weight: bold;
        }

        fieldset {
            border-radius: 4px;
            background: #fff;
            background: -moz-linear-gradient(#fff, #f9fdff);
            background: -o-linear-gradient(#fff, #f9fdff);
            background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#fff), to(#f9fdff));/ background: -webkit-linear-gradient(#fff, #f9fdff);
            padding: 20px;
            border-color: rgba(4, 129, 177, 0.4);
        }

        input,
        textarea {
            color: #373737;
            background: #fff;
            border: 1px solid #CCCCCC;
            color: #aaa;
            font-size: 14px;
            line-height: 1.2em;
            margin-bottom: 15px;

            -moz-border-radius: 4px;
            -webkit-border-radius: 4px;
            border-radius: 4px;
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1) inset, 0 1px 0 rgba(255, 255, 255, 0.2);
        }

        input[type="text"],
        input[type="password"] {
            padding: 8px 6px;
            height: 22px;
            width: 280px;
        }

        input[type="text"]:focus,
        input[type="password"]:focus {
            background: #f5fcfe;
            text-indent: 0;
            z-index: 1;
            color: #373737;
            -webkit-transition-duration: 400ms;
            -webkit-transition-property: width, background;
            -webkit-transition-timing-function: ease;
            -moz-transition-duration: 400ms;
            -moz-transition-property: width, background;
            -moz-transition-timing-function: ease;
            -o-transition-duration: 400ms;
            -o-transition-property: width, background;
            -o-transition-timing-function: ease;
            width: 380px;

            border-color: #ccc;
            box-shadow: 0 0 5px rgba(4, 129, 177, 0.5);
            opacity: 0.6;
        }

        input[type="submit"] {
            background: #222;
            border: none;
            text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.3);
            text-transform: uppercase;
            color: #eee;
            cursor: pointer;
            font-size: 15px;
            margin: 5px 0;
            padding: 5px 22px;
            -moz-border-radius: 4px;
            border-radius: 4px;
            -webkit-border-radius: 4px;
            -webkit-box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.3);
        }

        textarea {
            padding: 3px;
            width: 96%;
            height: 100px;
        }

        textarea:focus {
            background: #ebf8fd;
            text-indent: 0;
            z-index: 1;
            color: #373737;
            opacity: 0.6;
            box-shadow: 0 0 5px rgba(4, 129, 177, 0.5);
            border-color: #ccc;
        }

        .small {
            line-height: 14px;
            font-size: 12px;
            color: #999898;
            margin-bottom: 3px;
        }

        input[type=checkbox] {
            display: none;
        }

        input[type=checkbox]+label {
            display: inline-block;
            padding: 0 0 0 0px;
        }

        input[type=checkbox]:checked+label {
            width: 70px;
            height: 70px;
            border: black 3px solid;
            border-radius: 20px;

            display: inline-block;
            padding: 0 0 0 0px;
        }
    </style>

    <div id="wrapper">
        <form action="marscloudrun.php" method="post">
            <fieldset>
                <legend>DADES EMPRESA</legend>
                <div>
                    <input type="text" name="empresa" placeholder="NOM EMPRESA" />
                </div>
                <div class="panel-service" style="display: grid;grid-template-columns: repeat(3,1fr);grid-template-rows: repeat(2,1fr);">


                    <div>
                        <input type="checkbox" name="wordpress" id="img1" />
                        <label for="img1"><img src="images/wordpress.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="moodle" id="img2" />
                        <label for="img2"><img src="images/moodle.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="nextcloud" id="img3" />
                        <label for="img3"><img src="images/nextcloud.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="ftp" id="img4" />
                        <label for="img4"><img src="images/ftp.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="mediawiki" id="img5" />
                        <label for="img5"><img src="images/mediawiki.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="jira" id="img6" />
                        <label for="img6"><img src="images/jira.png" width="50" height="50" /></label>
                    </div>
                    <div>
                        <input type="checkbox" name="rocketchat" id="img7" />
                        <label for="img7"><img src="images/rocket.png" width="50" height="50" /></label>
                    </div>
                    <div></div>
                    <div>
                        <input type="checkbox" name="joomla" id="img8" />
                        <label for="img8"><img src="images/joomla.png" width="50" height="50" /></label>
                    </div>
                </div>

                <input type="submit" name="submit" value="Enviar" />
            </fieldset>
        </form>
    </div>
    <div class="mt-4">
        <a href="marscloudlogout.php" class="btn btn-danger">Cierra la sesión</a>
    </div>
    <div class="underlay-photo"></div>
</body>

</html>