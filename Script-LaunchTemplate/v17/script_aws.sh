#!/bin/bash

export ip_empresa=$(curl 169.254.169.254/latest/meta-data/public-ipv4)

export INSTANCE_ID=$(curl http://169.254.169.254/latest/meta-data/instance-id)

export EMPRESA=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=EMPRESA" | jq .Tags[].Value | sed "s/\"//g")

export wordpress=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=wordpress" | jq .Tags[].Value | sed "s/\"//g")

export moodle=$(aws ec2 --region us-east-1 describe-tags --filters "Name=resource-id,Values=$INSTANCE_ID" "Name=key,Values=moodle" | jq .Tags[].Value | sed "s/\"//g")

envsubst < /home/base/record.json.base > /home/docker/record.json
aws route53 change-resource-record-sets --hosted-zone-id Z002189610XS6GC0V5JYV --change-batch file:///home/docker/record.json

docker network create red_$EMPRESA

envsubst < /home/base/proxy_invers/docker-compose.yml.base > /home/docker/proxy_invers/docker-compose.yml
docker-compose -f /home/docker/proxy_invers/docker-compose.yml up -d

if [ $wordpress == "true" ]
then
envsubst < /home/base/wordpress/docker-compose.yml.base_proxy > /home/docker/wordpress/docker-compose.yml
docker-compose -f /home/docker/wordpress/docker-compose.yml up -d
fi

if [ $moodle == "true" ]
then
envsubst < /home/base/moodle/docker-compose.yml.base_proxy > /home/docker/moodle/docker-compose.yml
docker-compose -f /home/docker/moodle/docker-compose.yml up -d
fi