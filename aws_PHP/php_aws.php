template AWS PHP

/*
// Cargar vendor AWS
require 'aws/aws-autoloader.php';

// Usar modulo de cliente EC2
use Aws\Ec2\Ec2Client;

// Crear nuevo cliente que cogerá las credenciales de .aws/credentials (usar siempre region us-east-1)
$client = new Aws\Ec2\Ec2Client([
    'region' => 'us-east-1',
    'version' => '2016-11-15',
    'profile' => 'default'
]);

// Ejecutar run-instances
$result = $client->runInstances([  
// Definir ID de launch template 
'LaunchTemplate' => [
    'LaunchTemplateId' => 'lt-00f47af9cb356b1e2'        
],
// Definir numero de instancias a levantar
'MaxCount' => 1,
'MinCount' => 1,
// Definir tags que cogeran valores de variables recogidas de formulario
'TagSpecifications' => [
    [
        'ResourceType' => 'instance',
        'Tags' => [
            [
                'Key' => 'EMPRESA',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'Name',
                'Value' => $EMPRESA,
            ],
            [
                'Key' => 'wordpress',
                'Value' => $wordpress,
            ],
            [
                'Key' => 'moodle',
                'Value' => $moodle,
            ],
            [
                'Key' => 'nextcloud',
                'Value' => $nextcloud,
            ],
            [
                'Key' => 'ftp',
                'Value' => $ftp,
            ],
        ],
    ],
],
]);
*/